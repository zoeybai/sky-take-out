package com.sky.handler;

import com.sky.constant.MessageConstant;
import com.sky.exception.BaseException;
import com.sky.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * 全局异常处理器，处理项目中抛出的业务异常
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 捕获业务异常
     * @param ex
     * @return
     */
    @ExceptionHandler
    public Result exceptionHandler(BaseException ex){
        log.error("异常信息：{}", ex.getMessage());
        return Result.error(ex.getMessage());
    }

    /**
     * 处理sql异常
     * @param ex
     * @return
     */
    @ExceptionHandler
    public Result exceptionHandler(SQLIntegrityConstraintViolationException ex){
        // 捕获到异常信息的字符串
        String  message = ex.getMessage();
        // 当字符串包含“重复条目”内容时
        if (message.contains("Duplicate entry")){
            // 获取重复条目异常信息中的用户名字段
            String[] split = message.split(" ");
            String username = split[2];
            // 返回用户名+已重复的错误响应结果（使用常量类中的枚举类型）
            return Result.error(username+ MessageConstant.ALREADY_EXISTS);
        }else {
            // 返回未知错误的响应结果（使用常量类中的枚举类型）
            return Result.error(MessageConstant.UNKNOWN_ERROR);
        }
    }

}
